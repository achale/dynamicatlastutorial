# dynamicatlasTutorial

Dynamic Atlas tutorial for creating choropleth maps.

This tutorial can be viewed at https://achale.gitlab.io/dynamicatlastutorial/.

This tutorial supports the Dynamic Atlas open-source software which can be downloaded from https://gitlab.com/achale/dynamicatlas 
and viewed at https://achale.gitlab.io/dynamicatlas/.

Helper functions for configuring the Dynamic Atlas using Python are provided at https://gitlab.com/achale/dhaconfig however it would be advisable to follow this tutorial and build a test website before attempting to use these functions.
